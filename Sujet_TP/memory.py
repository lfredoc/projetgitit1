import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
     """
	La fonction permet de melanger des cartes

	Elle fonctionne par l'interversion des elements d'une meme liste de maniere aleatoire

	:param Tab: liste contenant les cartes du jeu à intervertir 
	:type Tab: list(char())
	:return: la liste avec les memes cartes mais melangees
	:rtype: list(char())
     """

    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
       	La fonction permet de creer une liste de meme longueur que celle passee en argument

	:param Tab: Liste contenant les cartes du jeu
	:type Tab: list(char())
	:return: Une liste vide de même taille que celle passee en argument
	:rtype: list()
    """


def choisir_cartes(Tab):
    """
       	La fonction permet au joueur de choisir 2 cartes

	Elle demande a l'utilisateur de choisir les 2 cartes qu'il veut retourner,
	la fonction gere le cas ou la meme carte a ete choisie et redemande un choix.

	:param Tab: Liste contenant les cartes du jeu
	:type Tab: list(char())
	:return: Une liste qui contient les indices correspondants aux 2 cartes choisies
	:rtype: list(int, int)
    """
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
	La fonction permet de reveler les cartes entrees en parametre dans la liste des cartes cachees

	:param c1: indice de la carte 1
	:type c1: int
	:param c2: indice de la carte 2
	:type c2: int
	:param Tab: Liste contenant les cartes du jeu
	:type Tab: list(char())
	:param Tab_cache: Liste contenant les cartes du jeu cachees
	:type Tab_cache: list(char())
	:return: Une liste qui contient les cartes cachees
	:rtype: list(char())
    """

def jouer(Tab):
   """
	La fonction permet de jouer une partie de memory

	Dans un premier temps il y a un affichage des règles, un melange de cartes
	puis les tours de jeu s'enchaines 

	:param Tab: Liste contenant les cartes du jeu
	:type Tab: list(char())
	:return: la fonction ne return rien
	:rtype: void
    """

    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

#test
print("Le tableau initial est :", Tabl)
print("Le tableau modifié est :", melange_carte(Tabl))

#jouer(Tabl)
